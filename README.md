# vuex-modal-example

> A example project using Vuex + Semantic-UI to show/hide modal

![Result](https://storage.jumpshare.com/preview/f5UOAcHRHUSTzZgNbBA2kkB2VJpiH5n8MhYtoExLFx8BbcGpDGHwwPmmT0NSgbD0GhgBGssv4VcIRfArQuCV2N0Iq-_ZMIwlJNqsu6s4bO0F1kR3dMUjedqC16uBUu85)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```
*[Click here to subscribe and get more about Vue.js][0]*

[0]: https://www.getrevue.co/profile/ridermansb
