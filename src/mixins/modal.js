export default {
  mounted () {
    let self = this
    this.$nextTick(function () {
      self.$modal = $(this.$el)
    })
  },
  methods: {
    close () {
      this.$modal.modal('hide')
    }
  },
  props: {
    show: {
      type: Boolean,
      require: true,
    },
    displayClose: {
      type: Boolean,
      'default': true
    }
  },
  watch: {
    show (val) {
      if (val && !this.$modal.modal('is active')) {
        this.$modal.modal('show')
      }
    }
  }
}